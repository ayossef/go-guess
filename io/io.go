package io

import (
	"fmt"
	"strconv"
)

func displaymsg(msg string) {
	fmt.Println(msg)
}

func Displaycongrats(trails int) {
	displaymsg("congratulations !!  your score is " + strconv.Itoa(trails))
}

func Displaygohigher() {
	displaymsg("Unlucky guessing, go higher")
}
func Displaygolower() {
	displaymsg("Unlucky guessing, go lower")
}

func Readinput() int {
	var num int
	fmt.Println("Enter your guessing")
	fmt.Scanf("%d", &num)
	return num
}
func ReadUserInput() string {
	var username string
	fmt.Println("You are a winner please enter your name")
	fmt.Scanf("%s", &username)
	return username

}
