package game

import (
	"math/rand"
	"strconv"
	"strings"

	"gitlab.com/ayossef/go-guess/fileops"
	"gitlab.com/ayossef/go-guess/io"
)

const scoresFileName = "highscore.txt"
const Numberoftraisl = 3

var magicnum int
var remainingtrails int

func Newgame() {
	magicnum = rand.Intn(1)
	remainingtrails = Numberoftraisl
}

func Trail() bool {
	var num = io.Readinput()
	var result = compare(num)
	if !result {
		remainingtrails -= 1
	}
	return result
}

func compare(num int) bool {
	if isEqual(num) {
		io.Displaycongrats(remainingtrails)
		comapareScores(remainingtrails, io.ReadUserInput())
		return true
	}

	if isHigher(num) {
		io.Displaygolower()
		return false
	}
	if isLower(num) {
		io.Displaygohigher()
		return false
	}
	return false
}

func isEqual(num int) bool {
	return num == magicnum
}

func isLower(num int) bool {
	return num < magicnum
}

func isHigher(num int) bool {
	return num > magicnum
}

func readOldScore() (string, int) {
	highscore := fileops.ReadTextFile(scoresFileName)

	scoreparts := strings.Split(highscore, ",")
	oldUsername := scoreparts[0]
	oldUserScore, _ := strconv.Atoi(scoreparts[1])

	return oldUsername, oldUserScore
}
func comapareScores(newSCore int, newUsername string) {
	// obtain the old score
	_, oldScore := readOldScore()
	if newSCore >= oldScore {
		updateScore(newUsername, newSCore)
	}
}
func updateScore(newusername string, newscore int) {
	fileops.WriteTextFile(scoresFileName, newusername+","+strconv.Itoa(newscore))
}
